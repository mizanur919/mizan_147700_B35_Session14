<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Book Title</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>
<div class="container">
    <div class="wrap col-md-6 col-md-offset-3">
        <h3 class="heading">Book Title</h3>
        <form action="">
            <div class="form-group">
                <label for="name" id="b-name">Book Name</label>
                <input type="text" name="" id="name" placeholder="Enter Your Book Name"                             class="b-name">
            </div>
            <div class="form-group">
                <label for="author" id="b-author">Author Name</label>
                <input type="text" name="" id="author" class="b-author" placeholder="Author Name">
            </div>
            <div class="form-group">
                <label for="category" id="b-category">Book Category</label>
                <input type="text" name="" id="category" class="b-category"  placeholder="Category">
            </div>
            <div class="all-button">
                <button type="button" class="btn btn-default same">Save</button>
                <button type="button" class="btn btn-default same">Save & Add New</button>
                <button type="button" class="btn btn-default same">Reset</button>
                <button type="button" class="btn btn-default same">Back to List</button>
            </div>
        </form>
    </div>
</div>


<script src="js/bootstrap.min.js"></script>
</body>
</html>